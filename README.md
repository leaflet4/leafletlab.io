Leaflet blog website using [Hugo](https://gohugo.io) and hosted with [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/),
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. Install `go` and [hugo](https://gohugo.io/getting-started/installing/).
1. Preview your project:

   ```shell
   hugo server
   ```

1. Generate the website:

   ```shell
   hugo
   ```

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

# License

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
