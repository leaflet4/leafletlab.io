---
date: 2024-04-09T06:00:08Z
title: "About"
description: "About Leaflet Project"
featured_image: '/images/Victor_Hugo-Hunchback.jpg'
menu:
  main:
    weight: 1
---

Leaflet is a project to build a FOSS, p2p social network with community
moderation.
