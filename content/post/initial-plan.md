---
date: 2024-04-09T05:00:00Z
description: "Initial design of leaflet"
featured_image: ""
tags: ["design"]
title: "Initial architecture plan"
---

This is largely a brain dump of how I think Leaflet could be architected.

## Network Architecture

Most p2p applications I've seen are based around [IPFS](https://www.ipfs.com/)
or [libp2p](https://libp2p.io/) (which IPFS is built on), but neither provides
a good mechanism for the kind of structured data I'm looking for in a link
aggregator service. Maybe I just don't understand them well enough, but I've
also seen enough complaints about performance that I decided to look around
and see what's available.

Over the last year or so, I've been watching [iroh](https://iroh.computer/),
which is a project that was initially intended to be a Rust implementation of
IPFS, but its creators thought they could do better. As a very brief overview,
here is Iroh's tag line:

> Iroh is a protocol for syncing bytes.
> Bytes of any size, across any number of devices.
> Use iroh to create apps that scale different.

But what's interesting is a few of the specific design choices. Since Iroh wants
to be a modular application, it exposes a lot of its internals that I think can
be used to great effect in Leaflet. Iroh has three main concepts, which they
"layers":

- documents - syncable key-value store with multiple readers and writers
- blobs - opaque bytes identified by a hash
- connections - connection layer with peers

But there's more to it:

- authors - has a keypair and all edits are signed by that keypair
- permissions - public key allows reads, private key allows writes
- range-based set reconciliation - saves bandwidth and improves performance

### Documents

Documents have a very interesting property in that posts are non-destructive,
and "versions" are "older" (wall clock) values posted to the same key.
This is interesting in a few ways:

- if a value is a comment, can easily see an edit log - no more ninja-edits
- if a value is a vote/flag, can see all of the users who voted/flagged the post
- times are wall-clock time, so no way to tell who's "first"

So it presents challenges as well as opportunities.

### Users

The Iroh developers recommend having an author be tied to a device and not
shared across devices, so a "user" would then have to somehow demonstrate
ownership of multiple author keys. This can probably be implemneted simply
enough with a shared users document where a user would create a post with
data signed by all private keys. When a new device is added, that post would
be signed by that new device.

Another option is to use some sort of external blockchain, but that sounds like
overkill.

## Moderation

In my research on distributed moderation systems, I found [TrustNet](https://cblgh.org/trustnet/),
which is based on Appleseed, described in the paper, "Propagation Models for Trust and Distrust in Social Networks".

Older Web of Trust networks are based on explicit trust, meaning: if A trusts B and B trusts C, then A trusts C.
Appleseed makes this a bit more fine-grained and introduces fractional trust.
So if A partially trusts B and B partially trusts C, then A would trust C less than B trusts C.
This means Leaflet can decide what trust means, and produce trust weights based
on some metric specific to how Leaflet operates.
Also, unlike web of trust systems, Appleseed provides a mechanism for distrust,
meaning negative trust, as well as a few options for tuning the algorithm.

TrustNet takes this a step further and introduces the concept of trust zones.
The main idea is that A may trust B for moderation, but not for music curation.

I think this general idea is pretty interesting, and TrustNet has some
real-world use in [Cabal](https://cabal.chat/), a distributed chat service.

Since moderation is a huge value proposition for Leaflet, stay tuned for more
posts about approaches and initial findings.

## Other interesting projects

I'm still looking at the existing space, but here are a few that caught my eye:

- [SimpleX](https://simplex.chat/) - maybe useful for live chat?
- [Freenet](https://freenet.org/) - successor to OG Freenet (now [hyphanet](https://www.hyphanet.org/pages/about.html)
- [Bittorrent File System](https://www.bittorrent.com/token/bittorrent-file-system) - huge network, maybe useful for image/video hosting?
- [scuttlebutt](https://www.scuttlebutt.nz/) - hyper-local social networking

## Supported Platforms

I intend to write this in Rust and React using [Tauri](https://tauri.app/),
and the initial target platform will be desktop Linux. Once the app is stable,
I'll set up the build toolchain to support Windows and macOS, and experiment
with mobile builds. Web can probably be supported with WebRTC, but that sounds
like a rabbit-hole that could derail my interest in the project and may be
something Iroh adds in the future.

My reasoning here is that desktops have a _lot_ more flexibility for things like
disk usage, networking, and installation. I can easily wrap up my app in a
flatpak for easy distribution, and other devs would likely be able to build for
whatever platform they want anyway.

Tauri already has experimental support for mobile, so porting shouldn't be a
huge effort once the underlying tech is working properly.

## Current status

I don't intend to release any code until I have at least proven the approach
for moderation. My reasoning here is that I don't want users playing with it
with no tools available for blocking unwanted content.

At this moment, here is what I've been working on:

- basic Tauri app w/ Iroh - over-complicated chat server
- Rust implementation of Appleseed, based on [appleseed-metric](https://github.com/cblgh/appleseed-metric) - will rewrite
- this blog system - mostly for my own notes, but could be useful for others

## Licensing

I intend to release the app under the AGPL, at least until I figure out what
exactly I want it to do. Any libraries I release will be under a much more
liberal license (probably MPL 2.0 or Apache 2.0, but maybe MIT or BSD), so if
I lose interest, they can be reused by anyone else who finds the approach
interesting.

## Conclusion

Stay tuned! I plan to post about the various papers I've been reading, as well
as walk through various aspects of the code I write in more detail.
