---
date: 2024-04-09T04:00:08Z
description: "Intro to Leaflet"
## featured_image: "/images/Pope-Edouard-de-Beaumont-1844.jpg"
tags: ["meta"]
title: "Intro to Leaflet"
---

Leaflet is a project I've thought about off and on for a while now, and this
blog is intended to document my musings about the project.

At its core, Leaflet is a peer-to-peer link aggregator site that's intended to
fill a similar niche as Reddit, with both distributed storage and moderation.
The motivation here is based on a few frustrations I've had with similar
projects.

## Other services

### Reddit

Reddit largely served my needs, but when they went forward with their decision
to charge for API access, third party apps were no longer viable. There's a lot
of drama around that, but at that point I really decidet that a centralized
social media website was not where I wanted to spend my time, since access to
content could be revoked at any time.

But aside from that, Reddit has other issues, such as:

- subreddit quality is based _way_ too much on the quality of the mods
- large amount of bots and spam, which goes back to the point about mods
- voting mechanism promotes first-movers and popularity over quality

I found that I was most happy with the medium sized subreddits because they
usually had minimal astroturfing and other forms of spam, probably because they
weren't popular enough to attract attention. However, they were popular enough
for motivated individuals to find. But to find one decent subreddit, I would
generally need to go through several unattractive subreddits (usually because
of poor mods) before finding a good fit.

### Lemmy

Lemmy is a [fediverse](https://www.fediverse.to/) set of services that's
intended to replace Reddit, and the design borrows a _lot_ from Reddit.
Due to this, it duplicates many of the issues Reddit has with moderation,
though the devs do seem to be actively interested in improving the algorithms
around post ranking.

But it inherits some undesirable aspects from both the nature of the fediverse
and Reddit, such as:

- moderation - you can at least appeal to your instance's admin (could be you!)
- susceptible to bots and spam - less of an issue so far because it's small
- voting - better than Reddit, but there's still risk of vote brigating
- scalability - I'm worried hosting costs will eventually kill off instances
- namespacing - users think in terms of community names, not instances

### Others

There are a bunch of similar competitors that I'm not going to mention.
In general, they fail for one reason or another, whether that's moderation,
performance, or limitations on account creation. Almost all are centralized,
and the few p2p projects are either designed to attract the "wrong" types
(i.e. illegal activities), are too exclusive (e.g. strict web-of-trust),
are ephemeral, or have some other undesirable (to me) trait.

## Leaflet Goals

So here's my wishlist for what I'd like this to become:

- not profit driven - profit motive seems to eventually kill projects like this
- user-controlled moderation - no bad mods to ruin your experience
- decent moderation UX - some kind of distributed trust system to share mod load
- persistent - unpopular content con disappear, but most content should stay
- safe - users should not be forced to store illegal content
- fast - should feel like a centralized web service
- single namespace

I'm still not exactly sure what the design will look like, but I'll detail some
of my ideas in upcoming blog posts.
